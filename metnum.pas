unit MetNum;

{$mode objfpc}

interface

uses
  Classes, SysUtils, ParseMath ,Grids,Dialogs,Math,
  FileUtil, Forms, Controls, ComCtrls,StdCtrls, ExtCtrls, Spin, Types;


type
    TMetNum=class
    private

    public
      //constructor create;
      function f(x:Real;s: String):Real;
      function f(x : Real;y : Real;s: String):Real;
      function Area(a:Real;b :Real;n:Integer; fx:String):Real;
      function Prueba():Integer;

  end;

var
  check: Boolean;

implementation

function TMetNum.f(x : Real;s: String):Real;
var MiParse: TParseMath;
begin
  MiParse:= TParseMath.create();
  MiParse.AddVariable('x',x);
  MiParse.Expression:= s;
  check:=false;
  try
    result:=MiParse.Evaluate();
  except
    begin
    ShowMessage('La funcion no es continua en el punto '+FloatToStr(x));
    check:=true;
    end;
  end;

  MiParse.destroy;
end;

function TMetNum.f(x : Real;y : Real;s: String):Real;
var MiParse: TParseMath;
begin
  MiParse:= TParseMath.create();
  MiParse.AddVariable('x',x);
  MiParse.AddVariable('y',y);
  MiParse.Expression:= s;
  check:=false;
  try
    result:=MiParse.Evaluate();
  except
    begin
    ShowMessage('La funcion no es continua en el punto '+FloatToStr(x)+FloatToStr(y));
    check:=true;
    end;
  end;

  MiParse.destroy;
end;



function TMetNum.Area(a:Real;b :Real; n:Integer; fx:String):Real;
var
  i :Integer;
  fa,fb,fr,h,sum: Real;
begin
  h:=(b-a)/n;
  sum:=0;

  fa:=f(a,fx);
  if check=true then exit;
  fb:=f(b,fx);
  if check=true then exit;


  for i:=1 to n-1 do
  begin
    sum:=sum + abs(f(a+i*h,fx));
    if check=true then exit;
  end;

  Result:=h*((abs(fa)+abs(fb))/2 + sum);

end;

function TMetNum.Prueba():Integer;
begin
     Result:=2;
end;


end.

